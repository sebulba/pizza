<?php

namespace frontend\controllers;

use frontend\models\Carts;
use frontend\models\CartItemProduct;
use yii\web\Cookie;
use common\models\User;

class CartController extends \yii\web\Controller
{

    private $user;

    public function initUserParam()
    {
        return $this->user = [
            'username' => md5(time()),
            'auth_key' => 'CF3U6eqsgEVEETlBQHWfOBLKgtvcNJWW',
            'password_hash' => '$2y$13$CfvNzMDfwB/yNxXx9QorSebBmIDQd8MpRnOiwHD37Z09TlbZW6rpi',
            'email' => md5(time()),
            'status' => 10,
            'created_at' => time() ,
            'updated_at' => time(),
        ];
    }

    public function actionIndex()
    {
//        зробити перевірку посту на тип товару
//        зараз всі продукти не опційні
//        $_POST = [
//            "product_id" => "1",
//            "product_count" => "1"
//        ];
//        if(1)
        if (\Yii::$app->request->isAjax)
        {
            $this->Add();
        }
    }

    public function actionShow()
    {
        $this->layout = 'contentOnly';
        return $this->render('_cart');
    }

    public function Add()
    {
        if(\Yii::$app->user->isGuest)
        {
            if (!isset(\Yii::$app->request->cookies['cart']))
            {
                $model = new User($this->initUserParam());
                $model->save();

                \Yii::$app->response->cookies->add(new Cookie(
                    [
                        'name' => 'cart',
                        'value' => $model->id,
                        'path' => '/'
                    ]));
            }
            else
            {
                $model = new User;
                $model = $model->findOne(\Yii::$app->request->cookies['cart']->value);
            }
            $product_id = htmlspecialchars($_POST['product_id']);
            $product_count = htmlspecialchars($_POST['product_count']);
            $cartModel = new Carts();

            $cartModel = $cartModel->find()->where(
                [
                    'user_id' => $model->id,
                    'status' => 0
                ])->one();
            if(!isset($cartModel))
            {
                $cartModel = new Carts(
                    [
                        'user_id' => $model->id,
                        'status' => 0
                    ]
                );
                $cartModel->save();
            }
            var_dump($_POST);die;
            $cartItemProductModel = new CartItemProduct();

            if($newItem = $cartItemProductModel->checkSimmilarItem($cartModel->id, $product_id))
            {
                $product_count = $newItem->count + $product_count;
                $newItem->setAttributes([
                    'cart_id' => $cartModel->id,
                    'product_id' => $product_id,
                    'count' => $product_count,
                ]);
                $cartItemProductModel = $newItem;
            }
            else
            {
                $cartItemProductModel->setAttributes([
                    'cart_id' => $cartModel->id,
                    'product_id' => $product_id,
                    'count' => $product_count,
                ]);
            }
            if($product_count > 0)
                $cartItemProductModel->save();
            else
                $cartItemProductModel->delete();
        }
    }

}
