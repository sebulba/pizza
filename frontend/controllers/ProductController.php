<?php
/**
 * Created by PhpStorm.
 * User: sebulba
 * Date: 20.02.16
 * Time: 22:18
 */

namespace frontend\controllers;

use backend\models\Products;

class ProductController extends \yii\web\Controller
{
    public function actionView($category,$pretty_url)
    {
        $product = new Products();
        $model = $product->getAdvProduct($category, $pretty_url);
        return $this->render(
            'view',
            [
                'product' => $model
            ]
        );
    }

}