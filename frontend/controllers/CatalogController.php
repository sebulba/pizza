<?php

namespace frontend\controllers;

use backend\models\Categories;
use backend\models\CategoriesImages;
use backend\models\Ingredients;
use backend\models\IngredientCategories;

class CatalogController extends \yii\web\Controller
{
    public function actionView($pretty_url)
    {
        $model = new Categories();
        $currentCat = $model->findOne(['pretty_url' => $pretty_url]);
        $image_model = new CategoriesImages();
        $image = $image_model->findOne(['category_id' => $currentCat['id']]);
        return $this->render('view',
            [
                'category' => $currentCat,
                'image' => $image,
                'productList' => $currentCat->getProductsList(),
            ]);
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCreatePizza()
    {
        $ingredient_cat_model = new IngredientCategories();
        $ingredientList = $ingredient_cat_model->getIngredientsList();

        if (\Yii::$app->request->isAjax)
        {
            var_dump($_POST);die;
        }
        return $this->render(
            'createPizza',
            [
                'model' => $ingredientList
            ]
            );
    }

}
