<?php
/**
 * Created by PhpStorm.
 * User: sebulba
 * Date: 20.02.16
 * Time: 22:25
 */
use yii\helpers\Url;
use yii\helpers\Html;
$parent = array_shift($product->getRelatedRecords())->pretty_url;
$this->title = $product->meta_title;
$this->params['breadcrumbs'][] = ['label' => 'Catalog', 'url' => [Url::toRoute(['catalog/'])]];
$this->params['breadcrumbs'][] = ['label' => $parent, 'url' => [url::to(['catalog/'.$parent])]];
$this->params['breadcrumbs'][] = $product->meta_title;
$image = $product->getImage();
//if(isset($_POST))
?>
<div class="product">
    <?=html::img('/'.$image->getPath('200x'))?>
    <?=$product->name?>
    <?=$product->price?>
</div>

