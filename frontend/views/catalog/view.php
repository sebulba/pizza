<?php

use yii\helpers\Url;
use yii\helpers\Html;
use frontend\components\CatalogChildWidget;
/* @var $this yii\web\View */
$this->title = $category->attributes['meta_title'];
$this->registerCssFile(
    "/css/CatalogChild.css",
    ['depends'=>'frontend\assets\AppAsset']
);
$this->registerJsFile(
    '/js/sendAjaxOrder.js',
    ['depends'=>'frontend\assets\AppAsset']
);
$this->params['breadcrumbs'][] = ['label' => 'Catalog', 'url' => [Url::toRoute(['catalog/'])]];
$this->params['breadcrumbs'][] = $this->title;
$attachedImage = $category->getImage();
?>
<div class="row">
    <div class="col-md-12"><h1><?=$category->attributes['meta_title']?></h1></div>
</div>
<div class="row">
    <div class="col-sm-7">
        <?=html::img(
            '/'.$attachedImage->getPath('200x'),
            [
                'alt' => $image->alt
            ]
        )?>
    </div>
    <div class="col-sm-5"><p><?=$category->attributes['description']?></p></div>
</div>
<div class="row">
    <?=CatalogChildWidget::widget([
        'productList' => $productList,
        'category' => $category
    ]); ?>
</div>