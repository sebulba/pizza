<?php
/**
 * Created by PhpStorm.
 * User: sebulba
 * Date: 02.03.16
 * Time: 4:01
 */
use frontend\components\CreateWidget;
use yii\helpers\Url;

$this->registerJsFile(
    '//code.jquery.com/ui/1.11.4/jquery-ui.js',
    ['depends'=>'frontend\assets\AppAsset']
);
$this->registerJsFile(
    '/js/createPizza.js',
    ['depends'=>'frontend\assets\AppAsset']
);
$this->registerCssFile(
    '//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css',
    ['depends'=>'frontend\assets\AppAsset']
);
$this->registerCssFile(
    '/css/createPizza.css',
    ['depends'=>'frontend\assets\AppAsset']
);
$this->title = 'Create Pizza';
$this->params['breadcrumbs'][] = ['label' => 'Catalog', 'url' => [Url::toRoute(['catalog/'])]];
$this->params['breadcrumbs'][] = $this->title;
 echo CreateWidget::widget(
     [
         'model' => $model
     ]
 );
?>
<div class="row ingredients-list-wrapper">
    <div class="col-sm-6 ingredients-list">
        <span class="ingredients-list-caption">
            Ваша піца включає:
        </span>
    </div>
</div>
