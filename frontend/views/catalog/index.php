<?php

use yii\helpers\Html;
use frontend\components\CatalogTileWidget;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Catalog';
$this->registerCssFile("/css/CatalogTile.css");
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row text-center">
        <?= html::a(
            html::img(
                'https://bcrc.bio.umass.edu/pedigreesoftware/sites/default/files/CREATE.jpg').
                    'Create Pizza',
                Url::toRoute(['catalog/create-pizza']))?>
    </div>
    <div class="row text-center">
        <?= CatalogTileWidget::widget([]); ?>
    </div>
</div>
