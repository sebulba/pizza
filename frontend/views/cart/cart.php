<?php
/**
 * Created by PhpStorm.
 * User: sebulba
 * Date: 26.02.16
 * Time: 0:26
 */
$totalSum = 0;
?>
<span class="shopping-cart-caption">Shopping cart</span>
<ul class="shopping-cart-list">
    <?php
    $totalSum = 0;
        foreach($model as $cartItem)
        {
            $product = $cartItem->getRelatedRecords()['product'];
            ?>
            <li class="shopping-cart-item">
                <form action="" class="set-count">
                    <input type="hidden" value="<?=$product->id?>" name="product_id">
                    <input type="hidden" name="value" value="-1">
                    <label data-type="minus" class="glyphicon glyphicon-minus red">
                        <input type="submit" name="value">
                    </label>
                </form>
                    <span class="sci-name"><?= $product->name?></span>
                    <span class="sci-price"><?= $product->price?></span>
                    <span class="sci-price"><?= $cartItem->count?></span>
                <form class="set-count">
                    <input type="hidden" value="<?=$product->id?>" name="product_id">
                    <input type="hidden" name="value" value="1">
                    <label data-type="plus" class="glyphicon glyphicon-plus green">
                        <input type="submit" name="value">
                    </label>
                </form>
            </li>
    <?php
            $totalSum += $product->price * $cartItem->count;
        }
    ?>
    <span class="shopping-cart-total">Total <?=$totalSum?> грн.</span>
</ul>