<?php
/**
 * Created by PhpStorm.
 * User: sebulba
 * Date: 03.03.16
 * Time: 5:49
 */

namespace frontend\components;

use yii\base\Widget;
use frontend\components;
use yii\helpers\Html;

class CreateWidget extends Widget
{
    public $model;

    public function init()
    {
        parent::init();
    }

    public function run()
    {

        $tabDivHtml = '';
        foreach($this->model as $key=>$ingredientCatItem)
        {
            $tabUlHtml[] = html::a(
                $ingredientCatItem->name,
                '#tabs-'.($key+1),
                [
                    'class' => 'tab-caption'
                ]
            );
//            echo $ingredientCatItem->name;
//            echo Cwidget::widget(
//                [
//                    'obj' => $ingredientCatItem
//                ]);
            foreach($ingredientCatItem->getRelatedRecords()['ingredients'] as $ingredient)
            {
                $ingredientAr[] =
                    html::img('/'.$ingredient->getImage()->getPath('50x')).
                        html::tag(
                            'span',
                            $ingredient->name,
                            [
                                'class' => 'icl-item-caption',
                                'data-id' => $ingredient->id
                            ]
                        ).
                    html::tag('div','',['class' => 'double']);
            }
            $tabDivHtml .= html::tag(
                'div',
                html::ul($ingredientAr,['class' => 'ingredient-category-list text-left']),
                [
                    'id' => 'tabs-'.($key+1)
                ]
            );

            unset($ingredientAr);
//            echo $key;
        }
//        print_r($tabUlHtml);
//        return Html::tag('div','', ['id' => 'tabs']);
        $createPizzaHtml = html::tag(
            'div',
            html::ul($tabUlHtml,['class' => 'tabs-list']).$tabDivHtml,
            [
                'id' => 'tabs'
            ]
        );
        return html::decode($createPizzaHtml);
    }
}