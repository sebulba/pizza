<?php
/**
 * Created by PhpStorm.
 * User: sebulba
 * Date: 18.02.16
 * Time: 2:18
 */
namespace frontend\components;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;


class CatalogChildWidget extends Widget
{
    public $productList;
    public $categoryListChildHtml;
    public $category;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        foreach($this->productList as $productItem)
        {
            $productItemLink = url::to(['product/'.$this->category->pretty_url.'/'.$productItem->pretty_url]);
            $img =  html::img('/'.$productItem->getImage()->getPath('x100'));
            $price = html::beginTag(
                'span',
                [
                    'class' => 'product-item-price'
                ]).$productItem->price.html::endTag('span');
            $description = html::tag('span',$productItem->description);
            $toOrder = html::beginForm("$productItemLink",'post', $options = ['class' => 'to-order']).
                html::submitButton ('Order', $options = [] ).
                html::hiddenInput ( 'id', $productItem->id ).
                html::hiddenInput ( 'count', 1 ).
                html::endForm();
            $itemCaption = html::beginTag(
                    'span',
                    [
                        'class' => 'product-item-caption'
                    ]
                    ).$productItem->name.html::endTag('span');

            $itemsAr[] = Html::a(
                $img.$itemCaption,
                $productItemLink,
                [
                    'class' => 'tile-item',
                ]).$price.$toOrder.$description;
        }
        $this->categoryListChildHtml = html::ul($itemsAr);
        Modal::begin([
            'header' => '<h2>BUY ME</h2>',
            'toggleButton' => [
                'label' => 'Order',
                'class' => 'hidden',
                'id' => 'productModal'
            ],
        ]);
        echo html::button(
            html::tag('span','',['class' => 'glyphicon glyphicon-minus']),
            [
                'type' => 'button',
                'class' => 'btn btn-default btn-number',
                'data-type' => 'minus',
                'data-field' => 'quant[1]'
            ]
        );
        echo html::input(
            'text',
            'quant[1]',
            '1',
            [
                'class' => 'form-control input-number count-cart',
                'readonly' => "readonly"
            ]
        );
        echo html::button(
            html::tag('span','',['class' => 'glyphicon glyphicon-plus']),
            [
                'type' => 'button',
                'class' => 'btn btn-default btn-number',
                'data-type' => 'plus',
                'data-field' => 'quant[1]'
            ]
        );
        echo '<br>';
        echo html::button('ok',[
            'type' => 'submit',
            'class' => 'modal-close',
            'id' => 'modalClose',
        ]);
        Modal::end();
        return html::decode($this->categoryListChildHtml);
    }
}