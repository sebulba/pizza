<?php
/**
 * Created by PhpStorm.
 * User: sebulba
 * Date: 26.02.16
 * Time: 0:08
 */
namespace frontend\components;

use frontend\models\CartItemProduct;
use frontend\models\Carts;
use yii\base\Widget;
use yii\helpers\Html;

class CartWidget extends Widget
{
    public function init()
    {
        parent::init();
    }

    public function run()
    {
        if($user_id = \Yii::$app->request->cookies['cart'])
        {
            $user_id = $user_id->value;
            $carts_model = new  Carts();
            $carts_model = $carts_model->findOne(['user_id' => $user_id]);
            $model = new CartItemProduct();
            $model = $model->getCartItem($carts_model->id);
//            попередньо невиправданий запит в модель Carts
//            $total_sum = 0;

//            foreach($model as $cartItem)
//            {
//                $product = $cartItem->getRelatedRecords()['product'];
//                $total_sum += $product->price * $cartItem->count;
//            }

//            $carts_model->setAttribute('total',$total_sum);
//            $carts_model->save();

            $cartHtmlContent = $this->render(
                '/cart/cart',
                [
                    'model' => $model,
//                    'carts_model' => $carts_model
                ]
            );
            return html::tag(
                'div',
                $cartHtmlContent,
                [
                    'class' => 'shopping-cart',
                    'id' => 'shoppingCart'
                ]
            );
        }

    }
}