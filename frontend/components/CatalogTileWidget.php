<?php
/**
 * Created by PhpStorm.
 * User: sebulba
 * Date: 18.02.16
 * Time: 2:18
 */
namespace frontend\components;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;
use backend\models\Categories;

class CatalogTileWidget extends Widget
{

    public $categoryListHtml;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $categories = new Categories();
//        $categoryList = $categories->find()->where(['activity'=>true])->all();
//        $categoryList = $categories->find()->joinWith('img')->orderBy('categories.id, image.itemId')->all();
        $categoryList = $categories->getCategoryList_img();
        $currSize = '300x';
//        foreach($categoryList as $categoryItem)
//        {
//            var_dump();
////            if($categoryItem->relatedRecords['image'] != null){
////                var_dump($categoryItem->relatedRecords->getPath($currSize));
////                echo '--------';
////            }
//        }
//        die;
        foreach($categoryList as $categoryItem)
        {
            $tileCaption = Html::beginTag(
                'span',
                [
                    'class' => 'tile-caption text-center',
                ]
            ).$categoryItem->name.Html::endTag('span');
            if($categoryItem->getRelatedRecords()['img'])
                $imgSrc = '/'.array_shift($categoryItem->getRelatedRecords()['img'])->getPath($currSize);
            else
                $imgSrc = '/images/cache/placeHolder/placeHolder_300x.png';
            $img = Html::img(
                $imgSrc,
                [
                    'class' => 'tile-img'
                ]
            );

//            $img = Html::img(
//                    $categoryItem->getImage()->getPath($currSize),
//                    [
//                        'class' => 'tile-img'
//                    ]
//                );
            $this->categoryListHtml .= Html::a(
                $tileCaption.$img,
                Url::toRoute(['view', 'pretty_url' => $categoryItem->pretty_url]),
                [
                    'class' => 'tile-item col-sm-6',
                ]
            );
        }

        return $this->categoryListHtml;
    }
}