<?php

namespace frontend\models;

use Yii;


/**
 * This is the model class for table "carts".
 *
 * @property integer $id
 * @property integer $user_id
 * @property boolean $status
 * @property integer $total
 */
class Carts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'carts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'total'], 'integer'],
            [['status'], 'boolean']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'status' => 'Status',
            'total' => 'Total',
        ];
    }
}
