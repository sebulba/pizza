<?php

namespace frontend\models;

use Yii;
use backend\models\Products;

/**
 * This is the model class for table "cart_item_product".
 *
 * @property integer $id
 * @property integer $cart_id
 * @property integer $product_id
 * @property integer $count
 */
class CartItemProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cart_item_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cart_id', 'product_id', 'count'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cart_id' => 'Cart ID',
            'product_id' => 'Product ID',
            'count' => 'Count',
        ];
    }

    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }

    public function getCartItemsProvider($cart_id)
    {
        return $this->find()->joinWith('product')->where(['cart_id' => $cart_id]);
    }

    public function getCartItem($cart_id = '-1')
    {
        return $this->getCartItemsProvider($cart_id)->orderBy('id')->all();
    }
//return NULL or record
    public function checkSimmilarItem($cart_id, $product_id)
    {
        return $this->findOne([
            'cart_id' => $cart_id,
            'product_id' => $product_id
        ]);
    }
}
