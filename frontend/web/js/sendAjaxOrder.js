/**
 * Created by sebulba on 26.02.16.
 */
function renderCart(html)
{
    if($('#shoppingCart').length)
    {
        $('#shoppingCart').replaceWith(html);
    }
    else
    {
        $('.shopping-cart-wrapper').append(html);
    }
}

function order(form)
{
    this.form = form;
    this.showCountDialog();
}

order.prototype.showCountDialog = function()
{
    $('#productModal').click();
    this.setProductAttr();
};

order.prototype.setProductAttr = function()
{
    $.post($(this.form).attr('action'),function(response)
    {
        $('.modal-dialog h2').append($(response).find('.product'));
    });

};

order.prototype.sendAjax = function(product_count)
{
    product_id = parseInt($(this.form).find('input[name="id"]').val());
    $.post( '/cart',{'product_id' : product_id, 'product_count' : product_count},function(response)
    {
        console.log(response);
        $.post('/cart/show',function(response)
        {
            renderCart(response);
        });
    });
    this.closeModal();
};

order.prototype.closeModal = function()
{
    $('.modal-header button[type="button"].close').click();
};

order.prototype.resetModal = function()
{
    $('.modal-dialog .product').remove();
    $('.count-cart').val('1');
};

$('.modal-header button[type="button"].close').click(function()
{
    orderItem.resetModal();
});

$('.modal-dialog').on('click','#modalClose',function()
{
    count = parseInt($('.modal-dialog .count-cart').val());
    orderItem.sendAjax(count);
});

$('.to-order').on('submit',function()
{
    orderItem = new order($(this));
    return false;
});

$('.modal-dialog .btn-number').click(function()
{

    currCountVal = parseInt($('.count-cart').val());
    switch($(this).attr('data-type'))
    {
        case 'plus':
            currCountVal++;
            break;
        case 'minus':
            if(currCountVal>0) currCountVal--;
            break;
    }
    $('.count-cart').val(currCountVal);
});

function setCount(form)
{
    this.form = form;
    this.data = this.form.serializeArray();
    $.post( '/cart',{'product_id' : this.data[0].value, 'product_count' : this.data[1].value},function(response)
    {
        $.post('/cart/show',function(response)
        {
            renderCart(response);
            $(this).find('input[type="submit"]').attr('disabled',false);
        });
    })
}

$('body').on('submit','.set-count',function()
{
    orderItem = new setCount($(this));
    $(this).find('input[type="submit"]').attr('disabled','disabled');
    return false;
});