/**
 * Created by sebulba on 03.03.16.
 */
function pizzaClass()
{
    this.data = {};
}

pizzaClass.prototype.pushItem = function(itemId,count,itemHtml)
{
    flag = true;

    for(key in this.data)
    {
        if(key == itemId && count != 2)
        {
            delete this.data[''+itemId+''];
            $(itemHtml).removeClass('active');
            flag = false;
            return false;
        }
    }

    if(flag)
    {
        this.data[''+itemId+''] = count;
        $(itemHtml).addClass('active');
        $(itemHtml).attr('data-count',count);
    }
};

//--for test post Ar
//need to remove [1]_BEGIN_
pizzaClass.prototype.testResponse = function()
{

    $.post('',pizzaObj.data,function(response)
    {
        console.log(response);
    });

};
//need to remove [1]_END_

$(document).ready(function()
{
    $( "#tabs" ).tabs();
    pizzaObj = new pizzaClass();

    $('.ingredient-category-list li').click(function()
    {
        itemId = $(this).find('span').attr('data-id');

        if(!$(this).attr('data-count'))
        {
            pizzaObj.pushItem(itemId,1,$(this));
        }
        else
        {
            count = ($(this).attr('data-count') % 2) + 1;
            pizzaObj.pushItem(itemId,count,$(this));
        }

        pizzaObj.testResponse();
    });
});
