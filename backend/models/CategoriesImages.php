<?php

namespace backend\models;

use rico\yii2images\models\Image;
use Yii;

/**
 * This is the model class for table "categories_images".
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $alt
 * @property boolean $activity
 * @property string $path
 *
 * @property Categories $product
 */
class CategoriesImages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file;

    public static function tableName()
    {
        return 'categories_images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id'], 'integer'],
            [['activity'], 'boolean'],
            [['alt'], 'string', 'max' => 255],
            [['path'], 'file', 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Product ID',
            'alt' => 'Alt',
            'activity' => 'Enable image',
            'path' => 'Path',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Categories::className(), ['id' => 'product_id']);
    }

    public function getImg()
    {
        return $this->hasOne(Image::className(), ['itemId' => 'category_id']);
    }

    public function getAdvImg($id){
        return $this->find()->joinWith('img')->where(['categories_images.category_id' => $id])->one();
    }
}
