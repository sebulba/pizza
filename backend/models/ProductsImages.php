<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "products_images".
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $alt
 * @property boolean $activity
 * @property string $path
 *
 * @property Products $product
 */
class ProductsImages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file;

    public static function tableName()
    {
        return 'products_images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['path'], 'file'],
//            [['product_id'], 'integer'],
//            [['activity'], 'boolean'],
//            [['alt'], 'string', 'max' => 255]

            [['path'], 'required'],
            [['product_id'], 'integer'],
            [['activity'], 'boolean'],
            [['alt'], 'string', 'max' => 255],
            [['path'], 'file', 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'alt' => 'Alt',
            'activity' => 'Enable image',
            'path' => 'Image',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }
}
