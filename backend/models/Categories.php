<?php

namespace backend\models;

use Yii;
use \rico\yii2images\behaviors\ImageBehave;
use \rico\yii2images\models\Image;
use backend\models\Products;

/**
 * This is the model class for table "categories".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $pretty_url
 * @property string $meta_keywords
 * @property string $meta_title
 * @property string $meta_description
 * @property boolean $activity
 */
class Categories extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            ImageBehave::className()
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','pretty_url'], 'required'],
            [['id'], 'integer'],
            [['activity'], 'boolean'],
            [['name'], 'string', 'max' => 255],
            [['description', 'pretty_url', 'meta_keywords', 'meta_title', 'meta_description'], 'string', 'max' => 1023]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'pretty_url' => 'Pretty Url',
            'meta_keywords' => 'Meta Keywords',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'activity' => 'Activity',
        ];
    }

    public function getImg()
    {
        return $this->hasMany(Image::className(), ['itemId' => 'id']);
    }

    public function getCategoryList_img()
    {
        return $this->find()->joinWith('img')->orderBy('categories.id, image.itemId')->all();
    }

    public function getProductsList()
    {
        return Products::find()->where(['parent_id' => $this->id])->all();
    }
}
