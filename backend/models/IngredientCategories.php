<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ingredient_categories".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Ingredients[] $ingredients
 */
class IngredientCategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ingredient_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngredients()
    {
        return $this->hasMany(Ingredients::className(), ['cat_id' => 'id']);
    }

    public function getIngredientsImages(){
        $ingredients_model = new Ingredients();
        return $ingredients_model->getIngredientsImages();
    }

    public function getIngredientsList()
    {
//        return $this->find()->joinWith('ingredients')->joinWith('ingredientsImages')->all();
        return $this->find()->joinWith('ingredients')->all();
    }
}
