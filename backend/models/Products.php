<?php

namespace backend\models;

use Yii;
use \rico\yii2images\behaviors\ImageBehave;

/**
 * This is the model class for table "products".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $description
 * @property string $price
 * @property boolean $recommend
 * @property string $name
 * @property string $pretty_url
 * @property string $meta_keywords
 * @property string $meta_title
 * @property string $meta_description
 * @property boolean $activity
 *
 * @property Categories $parent
 */
class Products extends \yii\db\ActiveRecord
{

    public function behaviors()
    {
        return [
            ImageBehave::className()
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','parent_id'], 'required'],
            [['parent_id'], 'integer'],
            [['price'], 'number'],
            [['recommend', 'activity'], 'boolean'],
            [['description', 'meta_keywords', 'meta_title', 'meta_description'], 'string', 'max' => 1023],
            [['name', 'pretty_url'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Category',
            'description' => 'Description',
            'price' => 'Price',
            'recommend' => 'Recommend',
            'name' => 'Name',
            'pretty_url' => 'Pretty Url',
            'meta_keywords' => 'Meta Keywords',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'activity' => 'Activity',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Categories::className(), ['id' => 'parent_id']);
    }

    public function getAdvProduct($category, $pretty_url)
    {
        return $this->find()->joinWith('parent')->where(
            [
                'categories.pretty_url' => $category,
                'products.pretty_url' => $pretty_url,
            ]
        )->one();
    }

}
