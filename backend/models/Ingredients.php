<?php

namespace backend\models;

use Yii;
use \rico\yii2images\behaviors\ImageBehave;

/**
 * This is the model class for table "ingredients".
 *
 * @property boolean $activity
 * @property string $name
 * @property integer $id
 * @property integer $price
 * @property integer $cat_id
 *
 * @property IngredientCategories $ingredientCat
 * @property IngredientsImages[] $ingredientsImages
 */
class Ingredients extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            ImageBehave::className()
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ingredients';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['activity'], 'boolean'],
            [['price', 'cat_id'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'activity' => 'Activity',
            'name' => 'Name',
            'id' => 'ID',
            'price' => 'Price',
            'cat_id' => 'Cat ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngredientCat()
    {
        return $this->hasOne(IngredientCategories::className(), ['id' => 'cat_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngredientsImages()
    {
        return $this->hasOne(IngredientsImages::className(), ['ingredient_id' => 'id']);
    }
}
