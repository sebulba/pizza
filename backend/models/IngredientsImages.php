<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ingredients_images".
 *
 * @property string $alt
 * @property boolean $activity
 * @property integer $id
 * @property integer $ingredient_id
 * @property string $path
 *
 * @property Ingredients $ingredient
 */
class IngredientsImages extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ingredients_images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['alt'], 'required'],
            [['activity'], 'boolean'],
            [['ingredient_id'], 'integer'],
            [['alt'], 'string', 'max' => 255],
            [['path'], 'file', 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'alt' => 'Alt',
            'activity' => 'Activity',
            'id' => 'ID',
            'ingredient_id' => 'Ingredient ID',
            'path' => 'Path',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngredient()
    {
        return $this->hasOne(Ingredients::className(), ['id' => 'ingredient_id']);
    }
}
