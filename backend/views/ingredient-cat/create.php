<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\IngredientCategories */

$this->title = 'Create Ingredient Categories';
$this->params['breadcrumbs'][] = ['label' => 'Ingredient Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ingredient-categories-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
