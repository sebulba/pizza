<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Ingredients */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ingredients-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'activity')->checkbox() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'cat_id')->textInput() ?>

    <?php echo '<span>Image</span>'?>
    <!--    --><?php //var_dump(1111);die; ?>

    <?= $form->field($image_model, 'alt')->textInput(['maxlength' => true]) ?>

    <?= $form->field($image_model, 'path')->fileInput(['multiple' => true, 'accept' => 'image/*'])?>

    <?= $form->field($image_model, 'activity')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
