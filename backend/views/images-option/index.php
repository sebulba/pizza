<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ImageSizeOptionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Image Size Options';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="image-size-option-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Image Size Option', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'width',
            'height',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
