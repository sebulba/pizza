<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\ImageSizeOption */

$this->title = 'Create Image Size Option';
$this->params['breadcrumbs'][] = ['label' => 'Image Size Options', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="image-size-option-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
