<?php

namespace backend\controllers;

use Yii;
use backend\models\CategoriesImages;
use backend\models\Categories;
use backend\models\CategoriesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;


/**
 * CategoriesController implements the CRUD actions for Categories model.
 */
class CategoriesController extends Controller
{
    public $path;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Categories models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CategoriesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Categories model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Categories model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Categories();
        $image_model = new CategoriesImages();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $image_model->load(Yii::$app->request->post());
            $image_model->file = UploadedFile::getInstance($image_model, 'path');
            $image_model->setAttribute('path',$image_model->file->name);
            $image_model->setAttribute('category_id',$model->id);
            if ($image_model->file && $image_model->validate())
            {
                $path = Yii::$app->params['uploadDir'].'/category_img/' . $image_model->file->baseName . '.' . $image_model->file->extension;
                $image_model->file->saveAs($path);
            }
            if($image_model->save())
            {
                Yii::$app->session->setFlash('success', 'Save Success');
            }
            else
            {
                Yii::$app->session->setFlash('error', 'Save failed');
            }
            $model->attachImage($path);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'image_model' => $image_model,
            ]);
        }
    }

    /**
     * Updates an existing Categories model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if($image = $model->getImage())
        {
            $model->removeImage($image);
        }
        $image_model = new CategoriesImages();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $image_model = $image_model->findOne(['category_id' => $id]);
            $image_model->load(Yii::$app->request->post());
            $image_model->file = UploadedFile::getInstance($image_model, 'path');
            $image_model->setAttribute('path',$image_model->file->name);
            if ($image_model->file && $image_model->validate()) {
                $path = Yii::$app->params['uploadDir'].'/category_img/' . $image_model->file->baseName . '.' . $image_model->file->extension;
                $image_model->file->saveAs($path);
            }
            if($image_model->save())
            {
                Yii::$app->session->setFlash('success', 'Update Success');
            }
            else
            {
                Yii::$app->session->setFlash('error', 'Update failed');
            }
            $model->attachImage($path);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'image_model' => $image_model->findOne(['category_id' => $id]),
            ]);
        }
    }

    /**
     * Deletes an existing Categories model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Categories model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Categories the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Categories::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
