<?php

namespace backend\controllers;

use Yii;
use backend\models\Ingredients;
use backend\models\IngredientsImages;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;


/**
 * IngredientsController implements the CRUD actions for Ingredients model.
 */
class IngredientsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Ingredients models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Ingredients::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Ingredients model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Ingredients model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
//        $model = new Ingredients();
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
//        } else {
//            return $this->render('create', [
//                'model' => $model,
//            ]);
//        }

        $model = new Ingredients();
        $image_model = new IngredientsImages();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $image_model->load(Yii::$app->request->post());
            $image_model->file = UploadedFile::getInstance($image_model, 'path');
//            var_dump(UploadedFile::getInstance($image_model,'path'));die;
            $image_model->setAttribute('path',$image_model->file->name);
            if ($image_model->file && $image_model->validate()) {
                $path = Yii::$app->params['uploadDir'].'/ingredient_img/' . $image_model->file->baseName . '.' . $image_model->file->extension;
                $image_model->file->saveAs($path);
            }
            if($image_model->save())
            {
                Yii::$app->session->setFlash('success', 'Save Success');
            }
            else
            {
                Yii::$app->session->setFlash('error', 'Save failed');
            }
            $image_model->setAttribute('ingredient_id',$model->getAttribute('id'));
            $image_model->save();
            $model->attachImage($path);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $IngredientItems = ArrayHelper::map(Ingredients::find()->all(), 'id', 'name');
            return $this->render('create', [
                'model' => $model,
                'image_model' => $image_model,
                'categoryItems' => $IngredientItems,
            ]);
        }
    }

    /**
     * Updates an existing Ingredients model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $image = $model->getImage();
        if($image)
        {
            $model->removeImage($image);
        }
        $image_model = new IngredientsImages();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $image_model = $image_model->findOne(['ingredient_id' => $id]);
            $image_model->load(Yii::$app->request->post());
            $image_model->file = UploadedFile::getInstance($image_model, 'path');
            $image_model->setAttribute('path',$image_model->file->name);
            if ($image_model->file && $image_model->validate()) {
                $path = Yii::$app->params['uploadDir'].'/ingredient_img/' . $image_model->file->baseName . '.' . $image_model->file->extension;
                $image_model->file->saveAs($path);
            }
            if($image_model->save())
            {
                Yii::$app->session->setFlash('success', 'Update Success');
            }
            else
            {
                Yii::$app->session->setFlash('error', 'Update failed');
            }
            $model->attachImage($path);
            return $this->redirect(['view','id' => $model->id]);


        } else {
            $categoryItems = ArrayHelper::map(Ingredients::find()->all(), 'id', 'name');
            return $this->render('update', [
                'model' => $model,
                'image_model' => $image_model->findOne(['ingredient_id' => $id]),
                'categoryItems' => $categoryItems,
            ]);
        }
    }

    /**
     * Deletes an existing Ingredients model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ingredients model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Ingredients the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ingredients::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
