<?php

namespace backend\controllers;

use backend\models\Categories;
use backend\models\ProductsImages;
use Yii;
use backend\models\Products;
use backend\models\ProductsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;

/**
 * ProductController implements the CRUD actions for Products model.
 */
class ProductController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Products models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductsSearch();
        $dataProvider = $searchModel->searchJoinCategory(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Products model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Products model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Products();
        $image_model = new ProductsImages();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $image_model->load(Yii::$app->request->post());
            $image_model->file = UploadedFile::getInstance($image_model, 'path');
            $image_model->setAttribute('path',$image_model->file->name);
            if ($image_model->file && $image_model->validate()) {
                $path = Yii::$app->params['uploadDir'].'/product_img/' . $image_model->file->baseName . '.' . $image_model->file->extension;
                $image_model->file->saveAs($path);
            }
            if($image_model->save())
            {
                Yii::$app->session->setFlash('success', 'Save Success');
            }
            else
            {
                Yii::$app->session->setFlash('error', 'Save failed');
            }
            $image_model->setAttribute('product_id',$model->getAttribute('id'));
            $image_model->save();
            $model->attachImage($path);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $categoryItems = ArrayHelper::map(Categories::find()->all(), 'id', 'name');
            return $this->render('create', [
                'model' => $model,
                'image_model' => $image_model,
                'categoryItems' => $categoryItems,
            ]);
        }
    }

    /**
     * Updates an existing Products model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $image = $model->getImage();
        if($image)
        {
            $model->removeImage($image);
        }
        $image_model = new ProductsImages();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $image_model = $image_model->findOne(['product_id' => $id]);
            $image_model->load(Yii::$app->request->post());
            $image_model->file = UploadedFile::getInstance($image_model, 'path');
            $image_model->setAttribute('path',$image_model->file->name);
            if ($image_model->file && $image_model->validate()) {
                $path = Yii::$app->params['uploadDir'].'/product_img/' . $image_model->file->baseName . '.' . $image_model->file->extension;
                $image_model->file->saveAs($path);
            }
            if($image_model->save())
            {
                Yii::$app->session->setFlash('success', 'Update Success');
            }
            else
            {
                Yii::$app->session->setFlash('error', 'Update failed');
            }
            $model->attachImage($path);
            return $this->redirect(['view','id' => $model->id]);


        } else {
            $categoryItems = ArrayHelper::map(Categories::find()->all(), 'id', 'name');
            return $this->render('update', [
                'model' => $model,
                'image_model' => $image_model->findOne(['product_id' => $id]),
                'categoryItems' => $categoryItems,
            ]);
        }
    }

    /**
     * Deletes an existing Products model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Products model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Products the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */

    protected function findModel($id)
    {
        if (($model = Products::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
